Scala SBT project using ScalaTest.

---


Implementation lives in `src/main/scala/BerlinClock.scala`

Tests live in `src/test/scala/BerlinClockSpec.scala`

---

You can see and run the test cases with `sbt test`

---

You create a `BerlinClock` with some specified hours, minutes, and seconds. It then calculates which lamps are turned on. You can then call `.render` to get a textual respresentation of the clock. See the Scaladoc or test cases for that method to understand the output.