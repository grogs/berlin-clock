import org.scalatest.{MustMatchers, FunSpec, FunSuite}

class BerlinClockSpec extends FunSpec with MustMatchers {

  describe("Invalid input is rejected") {
    it("No negative inputs are allowed") {
      intercept[AssertionError] { BerlinClock(-1,0,0) }
      intercept[AssertionError] { BerlinClock(0,-1,0) }
      intercept[AssertionError] { BerlinClock(0,0,-1) }
    }
    it("Don't go over max hour/minute/second") {
      intercept[AssertionError] { BerlinClock(24,0,0) }
      intercept[AssertionError] { BerlinClock(0,60,0) }
      intercept[AssertionError] { BerlinClock(0,0,60) }
    }

    //Could use ScalaCheck to generate random values outside valid range...
  }

  describe("Correct model is generated") {

    it("01:01:01") {
      val clock = BerlinClock(1,1,1)
      clock.top.numerator mustEqual 1
      clock.first.numerator mustEqual 0
      clock.second.numerator mustEqual 1
      clock.third.numerator mustEqual 0
      clock.forth.numerator mustEqual 1
    }

     it("00:00:00") {
      val clock = BerlinClock(0,0,0)
      clock.top.numerator mustEqual 0
      clock.first.numerator mustEqual 0
      clock.second.numerator mustEqual 0
      clock.third.numerator mustEqual 0
      clock.forth.numerator mustEqual 0
    }

    it("12:12:12") {
      val clock = BerlinClock(12,12,12)
      clock.top.numerator mustEqual 0
      clock.first.numerator mustEqual 2
      clock.second.numerator mustEqual 2
      clock.third.numerator mustEqual 2
      clock.forth.numerator mustEqual 2
    }

     it("03:02:01") {
      val clock = BerlinClock(3,2,1)
      clock.top.numerator mustEqual 1
      clock.first.numerator mustEqual 0
      clock.second.numerator mustEqual 3
      clock.third.numerator mustEqual 0
      clock.forth.numerator mustEqual 2
    }

    //Could do more...

  }


  describe("Clock is rendered correctly") {

    //If we were to change the output respresentation, we would have to rewrite these test cases. In that sense, it is more fragile than testing against the internal model.

    it("01:01:01") {
      val actual = BerlinClock(1,1,1).render
      val expected =
        s"""Y
           |____
           |R___
           |___________
           |Y___""".stripMargin
      expected mustEqual actual
   }

    it("00:00:00") {
      val actual = BerlinClock(0,0,0).render
      val expected =
        s"""_
            |____
            |____
            |___________
            |____""".stripMargin
      expected mustEqual actual
    }

    it("12:12:12") {
      val actual = BerlinClock(12,12,12).render
      val expected =
        s"""_
            |RR__
            |RR__
            |YY_________
            |YY__""".stripMargin
      expected mustEqual actual
    }

    it("03:02:01") {
      val actual = BerlinClock(3,2,1).render
      val expected =
        s"""Y
            |____
            |RRR_
            |___________
            |YY__""".stripMargin
      expected mustEqual actual
    }

    it("00:52:00 - Make sure we test the Red lamps on the third line") {
      val actual = BerlinClock(0,52,0).render
      val expected =
        s"""_
            |____
            |____
            |YYRYYRYYRY_
            |YY__""".stripMargin
      expected mustEqual actual
    }

  }

}
