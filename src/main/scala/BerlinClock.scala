import java.time.{LocalTime, LocalDate}

/**
  * We use a Fraction to represent a row of lamps
  * @param numerator The number of lamps that are turned on
  * @param denominator The total number of lamps on the row
  */
case class Fraction(numerator: Int, denominator: Int)

case class BerlinClock(hours: Int, minutes: Int, seconds: Int) {
  //Validate input
  assert(hours < 24 && hours >= 0)
  assert(minutes < 60 && minutes >= 0)
  assert(seconds < 60 && seconds >= 0)

  //Calculate each row of lamps, per the spec
  val top = Fraction(seconds % 2, 1)
  val first = Fraction(hours / 5, 4)
  val second = Fraction(hours % 5, 4)
  val third = Fraction(minutes / 5, 11)
  val forth = Fraction(minutes % 5, 4)

  /**
    * Render an instance of a BerlinClock as a multiline string.
    * It's quite trivial to change the output by changing the 'filler' and 'blank' streams that are passed to renderRow. E.g. you could produce colored output by including the relevant ASCI escape codes
    * Each lamp has a character, here a legend for the characters used:
    *   Y - Yellow lamp on
    *   R - Red lamp on
    *   _ - Underscore represents that a lamp is off (for either color)
    */
  def render = {
    import BerlinClock.renderRow
    //Third row is more exotic that the other because there are both Yellow and Red lamps.
    val thirdRowFill = Stream.from(5, 5).map{ i =>
      if (i % 15 == 0) "R" else "Y"
    }.iterator
    List(
      renderRow(top)("Y"),
      renderRow(first)("R"),
      renderRow(second)("R"),
      renderRow(third)(thirdRowFill.next()),
      renderRow(forth)("Y")
    ).mkString("\n")
  }
}

object BerlinClock {

  def renderRow(fraction: Fraction)(filler: => CharSequence, blank: => CharSequence = "_") = {
    Stream.continually(filler).take(fraction.numerator).toList ++
      Stream.continually(blank).take(fraction.denominator - fraction.numerator)
  }.mkString

  def now() = {
    val now = LocalTime.now()
    BerlinClock(now.getHour, now.getMinute, now.getSecond)
  }

}



